import os
import cv2
from cv2.data import haarcascades


def main():
    # choosing webcam
    cap = cv2.VideoCapture(0)

    # choosing face detention
    # these are taken from cv2 data folder
    detector = cv2.CascadeClassifier(os.path.join(
        haarcascades, 'haarcascade_frontalface_alt2.xml'))

    color = (0, 255, 0) #BGR
    stroke = 2

    while(True):
        # taking video from webcm
        _, frame = cap.read()

        # convert image to a grayscale image
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # detect faces
        faces = detector.detectMultiScale(
            gray, scaleFactor=1.5, minNeighbors=5)

        # draw boxes around detected faces
        for (_x, _y, _w, _h) in faces:
            cv2.rectangle(frame, (_x, _y), (_x+_w, _y+_h), color, stroke)

        # show output to user
        cv2.imshow('frame', frame)

        # press q to exit
        if cv2.waitKey(20) & 0xFF == ord('q'):
            break

    # releasing camera and cleaning
    cap.release()
    cv2.destroyAllWindows()


main()
