# Face Detection

## prerequesties
* python 3.6.7
* pipenv
* asdf preferred

## Setup
clone repository `git clone <repo-url>`
```shell
$ pipenv install
$ pipenv run python faces.py
```